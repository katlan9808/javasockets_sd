/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package delivery;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author Julian Daniel Gonzalez Castillo
 */
class MainServer {
     /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        Server serv = new Server(); //Se crea el servidor

        System.out.println("Starting server\n");
        serv.startServer(); //Se inicia el servidor
    }
}

class Server extends Connection {

    public Server() throws IOException {
        super("server");
    } //Se usa el constructor para servidor de Conexion

    public void startServer()//Método para iniciar el servidor
    {
        try {
            System.out.println("Waiting..."); //Esperando conexión

            clientSocket = serverSocket.accept(); //Accept comienza el socket y espera una conexión desde un cliente

            System.out.println("Client on line");

            //Se obtiene el flujo de salida del cliente para enviarle mensajes
            outClient = new DataOutputStream(clientSocket.getOutputStream());

            //Se le envía un mensaje al cliente usando su flujo de salida
            outClient.writeUTF("Request received and accepted");

            //Se obtiene el flujo entrante desde el cliente
            BufferedReader entrada = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            while ((serverMessage = entrada.readLine()) != null) //Mientras haya mensajes desde el cliente
            {
                //Se muestra por pantalla el mensaje recibido
                System.out.println(serverMessage);
            }

            System.out.println("end of connection");

            serverSocket.close();//Se finaliza la conexión con el cliente
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}

class Connection {

    private final int PORT = 1128; //Puerto para la conexión
    private final String HOST = "localhost"; //Host para la conexión
    protected String serverMessage; //Mensajes entrantes (recibidos) en el servidor
    protected ServerSocket serverSocket; //Socket del servidor
    protected Socket clientSocket; //Socket del cliente
    protected DataOutputStream outServer, outClient; //Flujo de datos de salida

    public Connection(String tipo) throws IOException //Constructor
    {
        if (tipo.equalsIgnoreCase("server")) {
            serverSocket = new ServerSocket(PORT);//Se crea el socket para el servidor en puerto 1128
            clientSocket = new Socket(); //Socket para el cliente
        } else {
            clientSocket = new Socket(HOST, PORT); //Socket para el cliente en localhost en puerto 1128
        }
    }
}
