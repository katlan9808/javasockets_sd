/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package delivery;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Julian Daniel Gonzalez Castillo
 */

class MainClient {
    public static void main(String[] args) throws IOException {
        Client cli = new Client(); //Se crea el cliente
        System.out.println("Starting client\n");
        cli.startClient();
    }
}


class Client extends Connection {

    public Client() throws IOException {
        super("client");
    } //Se usa el constructor para cliente de Conexion

    public void startClient() //Método para iniciar el cliente
    {
        try {
            //Flujo de datos hacia el servidor
            outServer = new DataOutputStream(clientSocket.getOutputStream());
            Scanner cnsl = new Scanner(System.in);
            //Inicio de programa leer datos
            Program progr = new Program();
            //Variable para ingreso de nuevos registros
            boolean newRegister = true;
            //Array que se escribira en el archivo
            ArrayList<String> linesText = new ArrayList<String>();
            while (newRegister) {
                System.out.println("Ingrese el numero de cuenta");
                String account = cnsl.nextLine();
                System.out.println("Ingrese el valor");
                String value = cnsl.nextLine();
                //Se agrega valor de cada registro al array
                linesText.add(account + "," + value);
                System.out.println("Registro grabado OK");
                System.out.println("¿Desea ingresar otro registro? S/N");
                //Validacion de ingreso nuevo registro
                newRegister = cnsl.nextLine().toUpperCase().equals("N") ? false : true;
            }
            //Se ejecuta la funcion de escribir en el archivo
            progr.writeText(linesText);
            //Se ingreesa valor de la cuenta a consultar
            System.out.println("Ingrese el numero de cuenta a consultar");
            String account = cnsl.nextLine();
            // se consulta total de cuenta
            String totalAccount = progr.readFile(account);
            //Se imprime valor de la cuenta
            System.out.println(totalAccount);

            clientSocket.close();//Fin de la conexión

        } catch (Exception e) {
            System.out.println("Registro grabado NO-OK" + e.getMessage());
        }
    }
}

class Program {

    public void writeText(ArrayList<String> textLines) {
        try {
            BufferedWriter fileOut = new BufferedWriter(
                    new FileWriter(new File("datos.txt")));

            for (String text : textLines) {
                fileOut.write(text);
                fileOut.newLine();
            }

            fileOut.close();
        } catch (IOException fileError) {
            System.out.println("Ha habido problemas: " + fileError.getMessage());
        }
    }

    public String readFile(String idAccount) {
        try {
            if (!(new File("datos.txt")).exists()) {
                System.out.println("No he encontrado fichero.txt");
                return "Error encontrando el archivo de texto";
            }
            String totalAccount = "0";
            BufferedReader file = new BufferedReader(new FileReader(new File("datos.txt")));
            String linea = null;
            int total = 0;
            //Recorremos archivo
            while ((linea = file.readLine()) != null) {
                //hacemos split por el separador (,)
                String[] item = linea.split(",");
                if (item[0].equals(idAccount)) {
                    total = total + Integer.parseInt(item[1]);
                }
            }
            file.close();
            //Se devuelve total de la cuenta
            totalAccount = String.valueOf(total);
            return totalAccount;
        } catch (IOException fileError) {
            return "Ha habido problemas: " + fileError.getMessage();
        }
    }

}

class Connection {
    private final int PORT = 21288; //Puerto para la conexión
    private final String HOST = "localhost"; //Host para la conexión
    protected String serverMessage; //Mensajes entrantes (recibidos) en el servidor
    protected ServerSocket serverSocket; //Socket del servidor
    protected Socket clientSocket; //Socket del cliente
    protected DataOutputStream outServer, outClient; //Flujo de datos de salida

    public Connection(String tipo) throws IOException //Constructor
    {
        if(tipo.equalsIgnoreCase("server"))
        {
            serverSocket = new ServerSocket(PORT);//Se crea el socket para el servidor en puerto 21288
            clientSocket = new Socket(); //Socket para el cliente
        }
        else
        {
            clientSocket = new Socket(HOST, PORT); //Socket para el cliente en localhost en puerto 21288
        }
    }
}


