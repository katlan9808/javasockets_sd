/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package server;

import java.io.IOException;

/**
 *
 * @author Julian Daniel Gonzalez Castillo
 */
public class MainServer {
     /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        Server serv = new Server(); //Se crea el servidor

        System.out.println("Starting server\n");
        serv.startServer(); //Se inicia el servidor
    }
}
