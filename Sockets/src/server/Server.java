/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package server;
import program.Connection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Julian Daniel Gonzalez Castillo
 */
public class Server extends Connection {
public Server() throws IOException{super("server");} //Se usa el constructor para servidor de Conexion

    public void startServer()//Método para iniciar el servidor
    {
        try
        {
            System.out.println("Waiting..."); //Esperando conexión

            clientSocket = serverSocket.accept(); //Accept comienza el socket y espera una conexión desde un cliente

            System.out.println("Client on line");

            //Se obtiene el flujo de salida del cliente para enviarle mensajes
            outClient = new DataOutputStream(clientSocket.getOutputStream());

            //Se le envía un mensaje al cliente usando su flujo de salida
            outClient.writeUTF("Request received and accepted");

            //Se obtiene el flujo entrante desde el cliente
            BufferedReader entrada = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            while((serverMessage = entrada.readLine()) != null) //Mientras haya mensajes desde el cliente
            {
                //Se muestra por pantalla el mensaje recibido
                System.out.println(serverMessage);
            }

            System.out.println("end of connection");

            serverSocket.close();//Se finaliza la conexión con el cliente
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
}
