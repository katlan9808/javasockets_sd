/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package program;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author Julian Daniel Gonzalez Castillo
 */
public class Program {

    public void writeText(ArrayList<String> textLines) {
        try {
            BufferedWriter fileOut = new BufferedWriter(
                    new FileWriter(new File("datos.txt")));

            for (String text : textLines) {
                fileOut.write(text);
                fileOut.newLine();
            }

            fileOut.close();
        } catch (IOException fileError) {
            System.out.println("Ha habido problemas: " + fileError.getMessage());
        }
    }

    public String readFile(String idAccount) {
        try {
            if (!(new File("datos.txt")).exists()) {
                System.out.println("No he encontrado fichero.txt");
                return "Error encontrando el archivo de texto";
            }
            String totalAccount = "0";
            BufferedReader file = new BufferedReader(new FileReader(new File("datos.txt")));
            String linea = null;
            int total = 0;
            //Recorremos archivo
            while ((linea = file.readLine()) != null) {
                //hacemos split por el separador (,)
                String[] item = linea.split(",");
                if (item[0].equals(idAccount)) {
                    total = total + Integer.parseInt(item[1]);
                }
            }
            file.close();
            //Se devuelve total de la cuenta
            totalAccount = String.valueOf(total);
            return totalAccount;
        } catch (IOException fileError) {
            return "Ha habido problemas: " + fileError.getMessage();
        }
    }

}
