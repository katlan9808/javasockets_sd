/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package client;

import java.io.IOException;

/**
 *
 * @author Julian Daniel Gonzalez Castillo
 */
public class MainClient {

    public MainClient() {
    }

    public static void main(String[] args) throws IOException {
        Client cli = new Client(); //Se crea el cliente
        System.out.println("Starting client\n");
        cli.startClient();
    }
}
