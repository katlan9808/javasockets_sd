/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package client;

import program.Connection;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import program.Program;

/**
 *
 * @author Julian Daniel Gonzalez Castillo
 */
public class Client extends Connection {

    public Client() throws IOException {
        super("client");
    } //Se usa el constructor para cliente de Conexion

    public void startClient() //Método para iniciar el cliente
    {
        try {
            //Flujo de datos hacia el servidor
            outServer = new DataOutputStream(clientSocket.getOutputStream());
            Scanner cnsl = new Scanner(System.in);
            //Inicio de programa leer datos
            Program progr = new Program();
            //Variable para ingreso de nuevos registros
            boolean newRegister = true;
            //Array que se escribira en el archivo
            ArrayList<String> linesText = new ArrayList<String>();
            while (newRegister) {
                System.out.println("Ingrese el numero de cuenta");
                String account = cnsl.nextLine();
                System.out.println("Ingrese el valor");
                String value = cnsl.nextLine();
                //Se agrega valor de cada registro al array
                linesText.add(account + "," + value);
                System.out.println("Registro grabado OK");
                System.out.println("¿Desea ingresar otro registro? S/N");
                //Validacion de ingreso nuevo registro
                newRegister = cnsl.nextLine().toUpperCase().equals("N") ? false : true;
            }
            //Se ejecuta la funcion de escribir en el archivo
            progr.writeText(linesText);
            //Se ingreesa valor de la cuenta a consultar
            System.out.println("Ingrese el numero de cuenta a consultar");
            String account = cnsl.nextLine();
            // se consulta total de cuenta
            String totalAccount = progr.readFile(account);
            //Se imprime valor de la cuenta
            System.out.println(totalAccount);

            clientSocket.close();//Fin de la conexión

        } catch (Exception e) {
            System.out.println("Registro grabado NO-OK" + e.getMessage());
        }
    }
}
